import React, { Component } from 'react';

export default class Spinner extends Component {
    render() {
        return (
            <>
                {this.props.visible && <div class="spinner-border spinner" role="status">
                    <span class="sr-only">Loading...</span>
                </div>}
            </>
        );
    }
}