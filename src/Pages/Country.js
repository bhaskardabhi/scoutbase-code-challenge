import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import Spinner from "./../Partials/Spinner";
import { Link } from "react-router-dom";

export default class Country extends Component {
    state = { country: {}, loading: false }

    componentDidMount = () => {
        var that = this;
        const client = new ApolloClient({
            uri: 'https://countries.trevorblades.com'
        });

        this.setState({ loading: true });

        client.query({
            query: gql`query {
                country(code: "${this.props.match.params.id}") {
                code
                name
                native
                phone
                currency
                continent {
                    name
                }
                languages {
                    name
                    native
                    rtl
                }
                }
            }`
        }).then(data => {
            console.log(data.data.country);
            this.setState({country: data.data.country, loading: false})
        })
        .catch(error => {
            that.setState({ loading: false });
            alert("There was error fetching data")
        });
    };

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-10 offset-1">
                        <Spinner visible={this.state.loading} />
                        <dl class="row mt-5">
                            <dt class="col-sm-3">Name</dt>
                            <dd class="col-sm-9">{this.state.country.name}</dd>

                            <dt class="col-sm-3">Continent</dt>
                            <dd class="col-sm-9">{this.state.country.continent ? this.state.country.continent.name: ''}</dd>

                            <dt class="col-sm-3">Languages</dt>
                            <dd class="col-sm-9">
                                {this.state.country.languages && this.state.country.languages.map((language, index) => (
                                    <span key={index} class="badge badge-secondary mr-1">{language.name} ({language.native})</span>
                                ))}
                            </dd>

                            <dt class="col-sm-3">Currency</dt>
                            <dd class="col-sm-9">{this.state.country.currency ?this.state.country.currency:''}</dd>

                            <dt class="col-sm-3">Phone</dt>
                            <dd class="col-sm-9">{this.state.country.phone ? '+'+this.state.country.phone : ''}</dd>

                        </dl>

                        <Link className="btn btn-primary" to={"/countries"}>View Countries</Link>
                    </div>
                </div>
            </div >
        );
    }
}