import React from 'react';
import { Link } from "react-router-dom";

function Home() {
    return (
        <div class="list-group">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-10 offset-1 mt-5">
                        <Link className="list-group-item list-group-item-action" to="/countries">Countries</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;