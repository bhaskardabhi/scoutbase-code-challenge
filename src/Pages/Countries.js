import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import Spinner from "./../Partials/Spinner";
import { Link } from "react-router-dom";

export default class Countries extends Component{
    state = {countries: [],loading: false}

    componentDidMount = () => {
        var that = this;
        const client = new ApolloClient({
            uri: 'https://countries.trevorblades.com'
        });

        this.setState({ loading: true });

        client.query({
            query: gql`{
                countries {
                    code
                    name
                    native
                    continent {
                        name
                    }
                    languages {
                        name
                        native
                    }
                }
            }`
        }).then(countries => this.setState({ countries: countries.data.countries, loading: false }))
        .catch(error => {
            that.setState({ loading: false });
            alert("There was error fetching data")
        });
    };

    render(){
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-10 offset-1">
                        <Spinner visible={this.state.loading} />
                        <table class="table mt-5">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Country</th>
                                    <th>Languages Spoken</th>
                                    <th>Continent</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.countries.map((country,index) => (
                                    <tr key={index}>
                                        <th scope="row">{index+1}</th>
                                        <td>{country.name}</td>
                                        <td>
                                            {country.languages.map((language, index) => (
                                                <span key={index} class="badge badge-secondary mr-1">{language.name} ({language.native})</span>
                                            ))}
                                        </td>
                                        <td>{country.continent.name}</td>
                                        <td>
                                            <Link className="btn btn-primary" to={"/countries/"+country.code}>View</Link>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div >
        );
    }
}