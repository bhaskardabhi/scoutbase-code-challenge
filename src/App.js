import React from 'react';
import './App.css';
import Header from "./Partials/Header";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Pages/Home";
import Countries from "./Pages/Countries";
import Country from "./Pages/Country";

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/countries/:id" component={Country} />
        <Route path="/countries" component={Countries} />
      </Switch>
    </Router>
  );
}

export default App;